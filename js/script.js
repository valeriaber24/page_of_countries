const countriesElem = document.querySelector('.list_countries')
const search = document.querySelector('.list__input')
const select = document.querySelector('.list__filter');
const regionName = document.querySelector('.region__name');
const countries = document.querySelectorAll('.list__country');
const toggleButton = document.querySelector('.list__toogle');

let allCountries = [];
let filteredCountries = [];

async function getCountry() {
    const apiUrl = await fetch("https://restcountries.com/v3.1/all");
    const res = await apiUrl.json();
    allCountries = res;
    filteredCountries = res;
    showCountries(filteredCountries);
}

getCountry();

function showCountries(data) {
    countriesElem.innerHTML = "";
    data.forEach(element => {
        const country = document.createElement('div');
        country.classList.add('list__country');
        country.innerHTML = ` <div class="list__country__img">
            <img src="${element.flags.svg}" alt="flag">
        </div>
        <div class="list__country__info">
            <h2>${element.name.common}</h2>
            <p><b>Population: </b>${element.population}</p>
            <p class="region__name"><b>Region: </b>${element.region}</p>
            <p><b>Capital: </b>${element.capital}</p>
        </div>
        </div>`;
        countriesElem.appendChild(country);
    });
}

function filterByRegion(region) {
    if (region === '0') {
        showCountries(allCountries);
        return;
    }
    filteredCountries = allCountries.filter(country => country.region === region);
    showCountries(filteredCountries);
}

select.addEventListener('change', (event) => {
    const region = event.target.value;
    filterByRegion(region);
});

function searchCountries() {
    const searchValue = search.value.toLowerCase();
    const regionValue = select.value;
    filteredCountries = allCountries.filter(country => {
        if (regionValue === '0' || country.region === regionValue) {
            return country.name.common.toLowerCase().includes(searchValue);
        }
        return false;
    });
    showCountries(filteredCountries);
}

search.addEventListener('input', searchCountries);

toggleButton.addEventListener('click', function () {
    const body = document.querySelector('body');
    const header = document.querySelector('.list_container:nth-child(1)');
    const button = document.querySelector('.list__toogle');
    const countries = document.querySelectorAll('.list__country');

    body.classList.toggle('night');
    header.classList.toggle('night');
    button.innerText = body.classList.contains('night') ? '☀️ Day Mode' : '🌙 Night Mode';

    countries.forEach(country => {
        country.classList.toggle('night');
    });
});
